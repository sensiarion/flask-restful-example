from pytest import fixture

from app.main import app


@fixture
def client():
    with app.test_client() as c:
        yield c


def generate_news():
    return {
        'title': 'oraora',
        'text': 'mudamudamudamudamudamuda',
        'is_visible': True
    }


@fixture()
def created_news(client) -> dict:
    news = generate_news()

    response = client.post('/api/v1/news', json=news, follow_redirects=True)

    return response.json


def test_news_create_success(client):
    news = generate_news()

    response = client.post('/api/v1/news', json=news, follow_redirects=True)

    assert response.status_code == 201


def test_news_update_success(client, created_news):
    sample = 'My sample title'
    created_news['title'] = sample
    response = client.post('/api/v1/news', json=created_news, follow_redirects=True)

    assert response.json['title'] == sample


def test_news_get_success(client, created_news):
    response = client.get(f'/api/v1/news/{created_news["id"]}', follow_redirects=True)

    assert response.status_code == 200


def test_news_get_single_not_found(client):
    response = client.get('/api/v1/news/-1', follow_redirects=True)

    assert response.status_code == 404


def test_news_get_list_success(client):
    response = client.get(f'/api/v1/news/?limit=20&offset=0', follow_redirects=True)

    assert type(response.json['news']) == list


def test_news_delete_success(client, created_news):
    response = client.delete(f'/api/v1/news/{created_news["id"]}', follow_redirects=True)

    assert response.json['deleted_at'] != None
