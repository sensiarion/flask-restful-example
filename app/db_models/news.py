from sqla_softdelete import SoftDeleteMixin
from sqlalchemy import Column, Integer, VARCHAR, Boolean
from sqlalchemy_serializer import SerializerMixin

from app.db_models.base import Base, TimestampMixin


class News(TimestampMixin, Base, SerializerMixin, SoftDeleteMixin):
    __tablename__ = 'news'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    title = Column(VARCHAR(256), nullable=False)
    text = Column(VARCHAR(9128), nullable=False)
    is_visible = Column(Boolean, server_default='false', nullable=False)
    is_created_by_human = Column(Boolean, server_default='false', nullable=False)
