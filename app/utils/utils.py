from typing import Callable, TypeVar, Union

import sqlalchemy as sa
from flask import g
from sqlalchemy import orm
from sqlalchemy.orm import Session

from app.config import Config
from app.db_models import Base
from app.exceptions import NotFound


def get_session() -> Session:
    """
    Получение сессии из глобального объекта.
    Перед вызовом необходимо удостоверится в инжекте сесии
    """
    return g.session


def create_session_factory() -> Callable[[], Session]:
    engine = sa.engine.create_engine(Config.db_connection_string)

    maker = orm.sessionmaker(engine)

    def get_session():
        session = maker()

        return session

    return get_session


db_session = create_session_factory()

RetrieveObject = TypeVar('RetrieveObject', bound=Base)


def retrieve_object(session: Session, object_class: RetrieveObject, id: Union[str, int]) -> RetrieveObject:
    """
    Получение объекта из базы данных при наличии
    :param session: сессия бд
    :param object_class: класс запрашиваемого объекта
    :param id: идентификатор в базе данных
    :raises NotFound: при отсутствии объекта в бд
    :return: найденную сущность при наличии
    """
    obj = session.query(object_class).get(id)
    if obj is None:
        raise NotFound(f"Unable to found entity {object_class.__tablename__} with {id=}")
    return obj
