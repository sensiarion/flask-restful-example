from flask import jsonify
from flask_restful import Resource, reqparse

from app.db_models import News
from app.utils.utils import get_session, retrieve_object

entity_parser = reqparse.RequestParser()
entity_parser.add_argument('title', required=True)
entity_parser.add_argument('text', required=True)
entity_parser.add_argument('is_visible', required=True, type=bool)

list_parser = reqparse.RequestParser()
list_parser.add_argument('limit', required=True, default=20, type=int)
list_parser.add_argument('offset', required=True, default=0, type=int)

out_fields = ['id', 'title', 'text', 'is_visible', 'created_at', 'deleted_at']


class NewsResource(Resource):

    def get(self, news_id: int):
        session = get_session()
        news = retrieve_object(session, News, news_id)

        return jsonify(news.to_dict(only=out_fields))

    def put(self, news_id: int):
        args = entity_parser.parse_args()

        session = get_session()
        news = retrieve_object(session, News, news_id)

        session.query(News).filter(News.id == news_id).update(**args)
        session.commit()

        return jsonify(news.to_dict(only=out_fields))

    def delete(self, news_id: int):
        session = get_session()

        news = retrieve_object(session, News, news_id)

        news.delete()
        data = news.to_dict(only=out_fields)

        session.commit()

        return data


class NewsListResource(Resource):
    def get(self):
        args = list_parser.parse_args()
        limit, offset = args['limit'], args['offset']
        session = get_session()

        result = session.query(News).order_by(News.created_at.desc()).limit(limit).offset(offset).all()
        values = [news.to_dict(only=out_fields) for news in result]
        return {'news': values}

    def post(self):
        args = entity_parser.parse_args()

        session = get_session()

        news = News(**args)
        session.add(news)
        session.commit()

        return news.to_dict(only=out_fields), 201
