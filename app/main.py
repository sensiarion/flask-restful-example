import logging

from flask import Flask, request, g
from flask_restful import Api
from sqlalchemy.orm import Session

from app.exceptions import NotFound
from app.routes.v1.news import NewsResource, NewsListResource
from app.utils.utils import db_session

app = Flask(__name__)
api = Api(app)
logger = logging.getLogger('app')

api.add_resource(NewsResource, '/api/v1/news/<int:news_id>')
api.add_resource(NewsListResource, '/api/v1/news/')


@app.errorhandler(NotFound)
def handle_not_found(e: NotFound):
    logger.error(f'Unable to found entity: {e}')
    return {'error': 'Entity not found'}, 404


@app.before_request
def inject_session():
    g.session = db_session()


@app.after_request
def close_session(response):
    session: Session = g.pop('session')
    if session:
        session.close()

    return response


if __name__ == '__main__':
    app.run('localhost', debug=True)
