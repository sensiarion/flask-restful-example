import os


class Config:
    db_connection_string = os.environ['CONNECTION_STRING']
